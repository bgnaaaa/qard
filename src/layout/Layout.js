import Header from "../components/header/Header";

export default function Layout({ children }) {
  return (
    <div>
      <header>
        {" "}
        <Header />{" "}
      </header>
      <main>{children}</main>
    </div>
  );
}
