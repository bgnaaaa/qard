import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
import React, { useState } from "react";
import "react-calendar/dist/Calendar.css";
import Header from "../../src/components/header/Header";
import { useRouter } from "next/router";
import MeetingCard from "../../src/components/card/MeetingCard";
export default function Meetings() {
  return (
    <>
      <Header />
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full pt-16 md:pt-28 md:w-1/2 bg-black h-full overflow-y-scroll ">
          <div className="w-full h-full flex justify-center px-7 pt-4 ">
            <div className="w-full max-w-lg relative">
              <Space h="lg" />
              <h1 className="text-xl text-white font-bold z-10">Meetings</h1>

              <Space h="lg" />
              <MeetingCard
                day_num="16"
                weekday_name="Mon"
                time="9:00 PM"
                meeting_name="Business Meeting"
                username="bagana"
                meeting_location="Ulaanbaatar, Bayanzurkh 14th
                Union Building, 5th floor, 501"
                meeting_status="Requested"
                meeting_status_color="#C8C348"
                className={"bg-[#C8C348]"}
              />
              <Space h="lg" />
              <MeetingCard
                day_num="17"
                weekday_name="Tue"
                time="9:00 PM"
                meeting_name="Business Meeting"
                username="bagana"
                meeting_location="Ulaanbaatar, Bayanzurkh 14th
                Union Building, 5th floor, 501"
                meeting_status="Accepted"
                meeting_status_color="#00F076"
                className={"bg-[#00F076]"}
              />
              <Space h="lg" />
              <MeetingCard
                day_num="18"
                weekday_name="Wed"
                time="9:00 PM"
                meeting_name="Business Meeting"
                username="bagana"
                meeting_location="Ulaanbaatar, Bayanzurkh 14th
                Union Building, 5th floor, 501"
                meeting_status="Canceled"
                meeting_status_color="#F50F0F"
                className={"bg-[#F50F0F]"}
              />
              <Space h="lg" />
              <MeetingCard
                day_num="19"
                weekday_name="Thu"
                time="9:00 PM"
                meeting_name="Business Meeting"
                username="bagana"
                meeting_location="Ulaanbaatar, Bayanzurkh 14th
                Union Building, 5th floor, 501"
                meeting_status="Sent you!"
                meeting_status_color="#7EBCED"
                className={"bg-[#7EBCED]"}
              />
              <div className="pb-10"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
