import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
export default function FinishAlert() {
  return (
    <>
      <div className="flex  bg-black h-screen overflow-hidden ">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full overflow-hidden md:items-center md:justify-center relative ">
          <div className="flex justify-start p-4 md:p-6">
            <button>
              <img
                src="/assets/images/qard-logo-white.png"
                alt=""
                className="w-[68px]"
              />
            </button>
          </div>

          <div className="px-7">
            <h1 className="text-[30px] leading-9 text-white font-bold px-4 pt-[25px]">
              BUILT QARD TOUGH
            </h1>
            <Space h="lg" />
            <h2 className="text-[20px] leading-7 text-white font-bold px-4 pt-[25px]">
              Get your QARD today.
            </h2>
            <Space h="lg" />
            <Link href="/signup">
              <button className="text-xl py-3 w-full mx-3 max-w-[290px] rounded-3xl bg-white text-black font-bold hover:bg-[#9264F3] hover:text-white border-white hover:border-white border-[2px]">
                Sign Up
              </button>
            </Link>
            <Space h="lg" />

            <div className="w-full h-[1px] bg-white rounded-3xl max-w-[320px]"></div>
            <Space h="lg" />
            <h2 className="text-[20px] leading-7 text-white font-bold px-4">
              Already have an account?
            </h2>
            <Space h="lg" />
            <Link href="/signin">
              <button className="text-xl py-3 w-full mx-3 max-w-[290px] rounded-3xl bg-[#9264F3] text-white font-bold border-[#9264F3] hover:border-[#9264F3] hover:bg-white hover:text-[#9264F3] border-[2px]">
                Sign In
              </button>
            </Link>
          </div>
          <Space h="lg" />
          <Space h="lg" />
          <Space h="lg" />

          <div
            className="min-h-full bg-center bg-cover top-0 sticky h-1/2 md:hidden"
            style={{
              backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
            }}
          >
            <div className="text-white font-bold p-10 text-4xl">QARD</div>
          </div>
        </div>
      </div>
    </>
  );
}
