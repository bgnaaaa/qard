import { Input, InputWrapper, Space } from "@mantine/core";
import { useRouter } from "next/router";
import Link from "next/link";
import MatteBlackCard from "../src/components/card/MatteBlackCard";
export default function Qard({ username = "bagana" }) {
  const router = useRouter();

  return (
    <>
      <div className="flex  bg-black h-screen overflow-hidden ">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full ">
          <div className="flex justify-between p-4 md:p-6">
            <button onClick={() => router.back()}>
              <img
                src="/assets/images/back-icon.svg"
                alt=""
                className="w-[50px] "
              />
            </button>
            <Link href="/home">
              <button>
                <img
                  src="/assets/images/qard-logo-white.png"
                  alt=""
                  className="w-[68px]"
                />
              </button>
            </Link>
          </div>
          <div className="w-full h-full flex justify-center px-7">
            <div className="w-full max-w-lg">
              <div className="text-xl md:text-2xl font-bold text-white">
                QARD Username: {username}
              </div>
              <Space h="lg" />
              <MatteBlackCard
                avatar="/assets/images/CP9010.png"
                name="Altanbagana"
                profession="Software Engineer"
                phonenumber="18817791682"
                email="1956226@tongji.edu.cn"
                website="tongji.edu.cn"
                address="No. 238 Gonghexin Road, Zhabei District, Zhongshan North Road, Shanghai, China, 200070"
                companylogo="/assets/images/qard-logo-white.png"
              />
              <Space h="lg" />

              <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                Done
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
