import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
export default function Signup() {
  return (
    <>
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full overflow-y-scroll">
          <div className="flex justify-between p-4 md:p-6">
            <button>
              <img
                src="/assets/images/qard-logo-white.png"
                alt=""
                className="w-[68px] "
              />
            </button>
            <Link href="/signin">
              <button className="font-bold text-lg text-white">Sign In</button>
            </Link>
          </div>
          <div className="w-full h-full flex justify-center px-7 pt-4">
            <div className="w-full max-w-lg">
              <div className="text-4xl md:text-5xl font-bold text-white pb-10 md:pb-16">
                Sign Up
              </div>
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold ">Username</div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="Username"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={<div className="pb-2 text-white font-bold">Email</div>}
              >
                <Input
                  className="bg-black"
                  placeholder="Email Address"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold">Password</div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="Password"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold">
                    Enter Password
                  </div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="Enter Password"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />

              <Space h="lg" />
              <div className="">
                <Link href="/createqard">
                  <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                    Next
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
