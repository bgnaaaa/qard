import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
import React, { useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import Recom from "../../src/components/recom/Recom";

import Header from "../../src/components/header/Header";
import { useRouter } from "next/router";
export default function Home() {
  const [value, onChange] = useState(new Date());
  const router = useRouter();
  return (
    <>
      <Header />
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full pt-20 md:pt-32 md:w-1/2 bg-black h-full overflow-y-scroll">
          <div className="w-full h-full flex justify-center px-7 pt-4">
            <div className="w-full max-w-lg">
              <div className="text-2xl md:text-3xl font-bold text-white z-10">
                Welcome Back! Altanbagana
              </div>
              <Space h="lg" />
              <h1 className="text-xl text-white font-bold z-10">My QARD</h1>
              {/* QARDS */}
              <Space h="lg" />
              <Link href="/qard">
                <div className="bg-[#28282B] border-white border-[5px] py-[54px] max-w-[340px] z-10 hover:border-[#9264F3]  rounded-3xl grid justify-items-center">
                  <h1 className="text-[24px] font-bold text-white z-10">
                    MY QARD
                  </h1>
                </div>
              </Link>
              <Space h="lg" />
              <h2 className="text-xl text-white font-bold z-10">Calendar</h2>
              <Space h="lg" />
              <div>
                <Calendar
                  onChange={onChange}
                  value={value}
                  className="rounded-3xl border-white border-[5px] hover:border-[#9264F3] z-10"
                />
              </div>
              <Space h="lg" />
              <h3 className="text-xl text-white font-bold z-10">Recommended</h3>
              <Space h="lg" />
              <div className="flex overflow-x-scroll gap-3">
                <Recom username="bagana" avatar="/assets/images/CP9010.png" />
                <Recom username="tugsuu" avatar="/assets/images/CP2020.png" />
                <Recom username="huslen" avatar="/assets/images/CP9911.png" />
                <Recom username="aisha" avatar="/assets/images/CP3030.png" />
                <Recom username="jack" avatar="/assets/images/CP9010.png" />
              </div>
              <div className="pb-10"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
