import "../styles/globals.css";

import Layout from "../src/layout/Layout";
import Head from "next/head";

function MyHome({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>QARD</title>
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}

export default MyHome;
