import { Input, InputWrapper, Space } from "@mantine/core";
import { useRouter } from "next/router";
import Link from "next/link";
export default function Step2() {
  const router = useRouter();

  return (
    <>
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full overflow-y-scroll">
          <div className="flex justify-between p-4 md:p-6">
            <button onClick={() => router.back()}>
              <img
                src="/assets/images/back-icon.svg"
                alt=""
                className="w-[50px] "
              />
            </button>
            <Link href="/">
              <button>
                <img
                  src="/assets/images/qard-logo-white.png"
                  alt=""
                  className="w-[68px]"
                />
              </button>
            </Link>
          </div>
          <div className="w-full h-full flex justify-center px-7 pt-4">
            <div className="w-full max-w-lg">
              <div className="text-2xl md:text-3xl font-bold text-white pb-10 md:pb-16">
                Create QARD - Step 2
              </div>
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold ">Company</div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="Company Name"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold">Your Jobe</div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="Job Title"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={<div className="pb-2 text-white font-bold">Streetr</div>}
              >
                <Input
                  className="bg-black"
                  placeholder="Street"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={<div className="pb-2 text-white font-bold">City</div>}
              >
                <Input
                  className="bg-black"
                  placeholder="City"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={<div className="pb-2 text-white font-bold">Country</div>}
              >
                <Input
                  className="bg-black"
                  placeholder="Country"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />
              <InputWrapper
                label={<div className="pb-2 text-white font-bold">Website</div>}
              >
                <Input
                  className="bg-black"
                  placeholder="www.yourwebsite.com"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />

              <Space h="lg" />
              <div className="pb-20">
                <Link href="/createqard/step3">
                  <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                    Next
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
