import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
export default function FinishAlert() {
  return (
    <>
      <div className="flex  bg-black h-screen overflow-hidden ">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full overflow-hidden">
          <div className="flex justify-end p-4 md:p-6">
            <button>
              <img
                src="/assets/images/qard-logo-white.png"
                alt=""
                className="w-[68px]"
              />
            </button>
          </div>

          <img src="/assets/images/bicycle-girl.png" alt="" className="" />
          <div className="px-7">
            <h1 className="text-[34px] leading-[40px] text-white font-bold px-4 pt-[25px]">
              YOUR QARD HAS BEEN CREATED!
            </h1>
            <Space h="lg" />
            <Link href="/home">
              <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                Enter Home
              </button>
            </Link>
          </div>
          <Space h="lg" />
          <img
            src="/assets/images/magician.png"
            alt=""
            className=" grid justify-end items-end overflow-hidden ml-20"
          />
        </div>
      </div>
    </>
  );
}
