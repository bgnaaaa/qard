import { Input, InputWrapper, Space } from "@mantine/core";
import { PasswordInput } from "@mantine/core";
import { EyeOff } from "tabler-icons-react";
import Link from "next/link";

export default function Signin() {
  return (
    <div className="flex  bg-black h-screen overflow-hidden ">
      <div
        className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
        style={{
          backgroundImage: `url("/assets/images/qard-signin-cover.jpeg")`,
        }}
      >
        <div className="text-white font-bold p-10 text-4xl">QARD</div>
      </div>

      <div className="w-full md:w-1/2 bg-black h-full ">
        <div className="flex justify-end p-4 md:p-6">
          <Link href="/signup">
            <button className="font-bold text-lg text-white">Sign Up</button>
          </Link>
        </div>
        <div className="w-full h-full flex justify-center pt-20 md:pt-24 px-6">
          <div className="w-full max-w-lg">
            <div className="text-4xl md:text-5xl font-bold text-white pb-10 md:pb-16">
              Sign In
            </div>
            <InputWrapper
              label={<div className="pb-2 text-white">Email or Username</div>}
            >
              <Input
                className="bg-black"
                placeholder="Enter Password"
                variant="filled"
                radius="lg"
                size={"lg"}
                styles={{
                  input: { fontSize: 14, fontWeight: 400 },
                  filledVariant: { color: "black" },
                  input: { color: "white" },
                }}
                classNames={{
                  wrapper:
                    " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                  defaultVariant: "defaultVariant-class",
                  filledVariant: "bg-black placeholder-gray-600",
                  unstyledVariant: "unstyledVariant-class",
                }}
              />
            </InputWrapper>

            <Space h="lg" />
            <InputWrapper
              label={<div className="pb-2 text-white">Password</div>}
            >
              <PasswordInput
                className="bg-black text-white"
                placeholder="Enter Password"
                variant="filled"
                radius="lg"
                size={"lg"}
                styles={{
                  input: { fontSize: 14, fontWeight: 400 },
                  filledVariant: { color: "black" },
                  input: { color: "white" },
                }}
                classNames={{
                  wrapper:
                    " rounded-2xl border-[3px] border-gray-600 hover:border-white ",
                  defaultVariant: "defaultVariant-class",
                  filledVariant: "bg-[#9264F3] placeholder-gray-600 text-white",
                  unstyledVariant: "unstyledVariant-class",
                }}
              />
            </InputWrapper>
            <Link href="/signin/resetpassword">
              <button className="text-sm text-[#9264F3] pt-2">
                Forgot Password?
              </button>
            </Link>
            <Space h="lg" />
            <Link href="/home">
              <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                Next
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
