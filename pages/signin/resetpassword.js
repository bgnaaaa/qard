import { Input, InputWrapper, Space } from "@mantine/core";

import Link from "next/link";

export default function Resetpassword() {
  return (
    <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
      <div
        className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
        style={{
          backgroundImage: `url("/assets/images/qard-signin-cover.jpeg")`,
        }}
      >
        <div className="text-white font-bold p-10 text-4xl">QARD</div>
      </div>

      <div className="w-full md:w-1/2 bg-black h-full overflow-y-scroll ">
        <div className="flex justify-end p-4 md:p-6">
          <Link href="/signup">
            <button className="font-bold text-lg text-[#9264F3]">
              Sign Up
            </button>
          </Link>
        </div>
        <div className="w-full h-full flex justify-center pt-20 md:pt-24 px-6">
          <div className="w-full max-w-lg">
            <div className="text-4xl md:text-5xl font-bold text-[#9264F3] pb-4 ">
              Forgot Password
            </div>
            <div className="text-sm md:text-md text-[#9264F3] pb-6 md:pb-12">
              Enter your registered email. We will send a one-time code to your
              email address.
            </div>
            <InputWrapper
              label={<div className="pb-2 text-white ">Email Address</div>}
            >
              <Input
                className="bg-black"
                placeholder="Enter Password"
                variant="filled"
                radius="lg"
                size={"lg"}
                styles={{
                  input: { fontSize: 14, fontWeight: 400 },
                  filledVariant: { color: "black" },
                  input: { color: "white" },
                }}
                classNames={{
                  wrapper:
                    " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                  defaultVariant: "defaultVariant-class",
                  filledVariant: "bg-black placeholder-gray-600",
                  unstyledVariant: "unstyledVariant-class",
                }}
              />
            </InputWrapper>

            <Space h="lg" />
            <button className="px-10 py-2 rounded-3xl bg-[#9264F3] text-white hover:bg-[#B4C3CC]">
              Send Verification
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
