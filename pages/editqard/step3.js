import { Input, InputWrapper, Space } from "@mantine/core";
import { useRouter } from "next/router";
import Link from "next/link";
export default function Step3() {
  const router = useRouter();
  return (
    <>
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full overflow-y-scroll">
          <div className="flex justify-between p-4 md:p-6">
            <button onClick={() => router.back()}>
              <img
                src="/assets/images/back-icon.svg"
                alt=""
                className="w-[50px] "
              />
            </button>
            <Link href="/">
              <button>
                <img
                  src="/assets/images/qard-logo-white.png"
                  alt=""
                  className="w-[68px]"
                />
              </button>
            </Link>
          </div>
          <div className="w-full h-full flex justify-center px-7 pt-4">
            <div className="w-full max-w-lg">
              <div className="text-2xl md:text-3xl font-bold text-white pb-10 md:pb-16">
                Edit QARD - Step 3
              </div>
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold ">
                    Name your QARD!
                  </div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="QARD Name"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />

              <div className="flex flex-col">
                <div className=" pb-2 text-white font-bold">
                  Upload your company logo here:
                </div>

                <button className="w-full py-10 border-gray-600 border-[3px] rounded-2xl grid justify-items-center">
                  <img
                    src="/assets/images/cloud-icon.svg"
                    alt=""
                    className="w-[100px]  "
                  />
                </button>
              </div>

              <Space h="lg" />

              <Space h="lg" />
              <div className="pb-[10px]">
                <Link href="/editqard/step4">
                  <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                    Next
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
