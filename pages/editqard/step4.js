import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
import { useRouter } from "next/router";
export default function Step4() {
  const router = useRouter();
  return (
    <>
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full overflow-y-scroll">
          <div className="flex justify-between p-4 md:p-6">
            <button onClick={() => router.back()}>
              <img
                src="/assets/images/back-icon.svg"
                alt=""
                className="w-[50px] "
              />
            </button>
            <Link href="/">
              <button>
                <img
                  src="/assets/images/qard-logo-white.png"
                  alt=""
                  className="w-[68px]"
                />
              </button>
            </Link>
          </div>
          <div className="w-full h-full flex justify-center px-7 pt-4">
            <div className="w-full max-w-lg">
              <div className="text-2xl md:text-3xl font-bold text-white pb-10 md:pb-16">
                Choose your QARD design
              </div>
              {/* MATTE BLACK */}
              <div className="bg-[#28282B] border-white border-[5px] py-[56px]  hover:border-[#9264F3]  rounded-3xl grid justify-items-center">
                <h1 className="text-[24px] font-bold text-white ">
                  MATTE BLACK
                </h1>
              </div>
              <Space h="lg" />
              {/* METALLIC SILVER */}
              <div className="bg-[#C0C0C0] border-white border-[5px] py-[60px]  hover:border-[#9264F3]  rounded-3xl grid justify-items-center">
                <h1 className="text-[24px] font-bold text-black ">
                  METALLIC SILVER
                </h1>
              </div>
              <Space h="lg" />
              {/* DULL GOLD */}
              <div className="bg-[#BBA14F] border-white border-[5px] py-[60px]  hover:border-[#9264F3]  rounded-3xl grid justify-items-center">
                <h1 className="text-[24px] font-bold text-black ">DULL GOLD</h1>
              </div>
              <Space h="lg" />
              {/* ROYALTY PURPLE */}
              <div className="bg-[#7851A9] border-white border-[5px] py-[60px]  hover:border-[#9264F3]  rounded-3xl grid justify-items-center">
                <h1 className="text-[24px] font-bold text-white ">
                  ROYALTY PURPLE
                </h1>
              </div>
              <Space h="lg" />

              <Space h="lg" />
              <div className="pb-20">
                <Link href="/editqard/finishalert">
                  <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                    Next
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
