import { Input, InputWrapper, Space } from "@mantine/core";
import Link from "next/link";
import React, { useState } from "react";
import "react-calendar/dist/Calendar.css";
import Header from "../../src/components/header/Header";
import { useRouter } from "next/router";
import ProfileCard from "../../src/components/card/ProfileCard";
export default function Home() {
  return (
    <>
      <Header />
      <div className="flex  bg-black h-screen md:overflow-hidden overflow-y-scroll">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full pt-20 md:pt-32 md:w-1/2 bg-black h-full overflow-y-scroll ">
          <div className="w-full h-full flex justify-center px-7 pt-4 ">
            <div className="w-full max-w-lg relative">
              <Space h="lg" />
              <h1 className="text-xl text-white font-bold z-10">My Profile</h1>
              {/* QARDS */}
              <Space h="lg" />

              <div className="bg-[#28282B] border-white border-[5px] py-[54px] max-w-[340px] z-10 hover:border-[#9264F3]  rounded-3xl grid justify-items-center">
                <h1 className="text-[24px] font-bold text-white z-10">
                  My QARD
                </h1>
              </div>
              <Space h="lg" />
              <Link href="/editqard/step1">
                <button className="px-6 py-1 bg-[#9264F3] text-white font-medium text-md rounded-2xl absolute right-0">
                  Edit
                </button>
              </Link>
              <div className="pt-10">
                <ProfileCard
                  username="bagana"
                  fullname="Altanbagana"
                  avatar="/assets/images/CP9010.png"
                />
              </div>

              <div className="pb-10"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
