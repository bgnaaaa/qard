import { Input, InputWrapper, Space } from "@mantine/core";
import { useRouter } from "next/router";
import Link from "next/link";
export default function EditUsername() {
  const router = useRouter();

  return (
    <>
      <div className="flex  bg-black h-screen overflow-hidden ">
        <div
          className="min-h-full bg-center bg-cover top-0 sticky w-1/2 hidden md:block"
          style={{
            backgroundImage: `url("/assets/images/qard-signup-cover.jpeg")`,
          }}
        >
          <div className="text-white font-bold p-10 text-4xl">QARD</div>
        </div>

        <div className="w-full md:w-1/2 bg-black h-full ">
          <div className="flex justify-between p-4 md:p-6">
            <button onClick={() => router.back()}>
              <img
                src="/assets/images/back-icon.svg"
                alt=""
                className="w-[50px] "
              />
            </button>
          </div>
          <div className="w-full h-full flex justify-center px-7 pt-4">
            <div className="w-full max-w-lg">
              <div className="text-2xl md:text-3xl font-bold text-white pb-10 md:pb-16">
                Edit Username
              </div>
              <InputWrapper
                label={
                  <div className="pb-2 text-white font-bold ">Username</div>
                }
              >
                <Input
                  className="bg-black"
                  placeholder="New Username"
                  variant="filled"
                  radius="lg"
                  size={"lg"}
                  styles={{
                    input: { fontSize: 14, fontWeight: 400 },
                    filledVariant: { color: "black" },
                    input: { color: "white" },
                  }}
                  classNames={{
                    wrapper:
                      " rounded-2xl border-[3px] border-gray-600 hover:border-[#9264F3]",
                    defaultVariant: "defaultVariant-class",
                    filledVariant: "bg-black placeholder-gray-600",
                    unstyledVariant: "unstyledVariant-class",
                  }}
                />
              </InputWrapper>
              <Space h="lg" />

              <button className=" py-3 w-full rounded-3xl bg-[#9264F3] text-white font-bold hover:bg-[#B4C3CC]">
                Done
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
